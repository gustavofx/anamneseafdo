<?php

/**
 * Fired during plugin activation
 *
 * @link       trudax.tech
 * @since      1.0.0
 *
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/includes
 * @author     Gustavo Texeira Rudiger <gustavo@trudax.tech>
 */
class Anamneseafdo_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
