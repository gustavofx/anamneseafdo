<?php

/**
 * Fired during plugin deactivation
 *
 * @link       trudax.tech
 * @since      1.0.0
 *
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/includes
 * @author     Gustavo Texeira Rudiger <gustavo@trudax.tech>
 */
class Anamneseafdo_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
