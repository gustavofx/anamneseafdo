<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       trudax.tech
 * @since      1.0.0
 *
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/public/partials
 */


// Include the main TCPDF library (search for installation path).
include( plugin_dir_path( __FILE__ ) .'../../lib/TCPDF/tcpdf.php');

$images_path = WP_PLUGIN_DIR . '/'. $this->plugin_name . '/images';

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Ateliê Furo de Orelhas');
$pdf->SetTitle('Ficha Anamnese');
$pdf->SetKeywords('TCPDF, PDF, Ficha Anamnese');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();
$style = array('width' => 1, 'cap' => 'round', 'join' => 'round', 'dash' => '0', 'color' => array(243, 205, 228));
$pdf->Rect(0, 0, 210, 5, 'DF', $style, array(243, 205, 228));
$pdf->Rect(0, 287, 210, 10, 'DF', $style, array(243, 205, 228));


// create some HTML content
$html = '
<div style="text-align:center">
    <img src="'.$images_path.'/logo_pdf.PNG" width="230" border="0"/>
</div>
<span style="background-color: #9263A9; color: white;">ANAMNESE DE CLIENTE</span>
<div>
    Nome Completo: ________________________________________________________________________________ <br/>
    Data de Nascimento:_______________________________ Idade: _________________ Sexo: ______________<br />
    RG:____________________________ CPF: _______________________ Profissão: ________________________<br />
    Telefone: ________________________________________ E-mail: _____________________________________<br />
    Endereço: ______________________________________________________________________________________<br />
    Bairro: _______________________ Cidade: _________________________________ UF: __________________<br />

    <div>
        <b>NOME DO BEBÊ</b>: _______________________________________________________________________<br />
        Data de Nascimento: __________________________ Idade: ________ Peso do Bebê: _______________<br />
        Nome do Pai: _________________________________ Nome da Mãe: ________________________________<br />
    </div>

    <div>
        <b>NOME DO MENOR</b>: ______________________________________________________________________<br />
        E-mail: ________________________________________________ Celular: __________________________<br />
        Data de Nascimento: __________________________ Idade: ______________________________________<br />
        Nome do Pai: _________________________________ Nome da Mãe: ________________________________<br />
    </div>

    <div>
        <p>( ) Autorizo a minha perfuração, estou ciente das instruções e obrigo-me a segui-las os cuidados pós aplicação do proﬁssional abaixo qualiﬁcado, todas as informações referentes ao procedimento utilizado, aos cuidados a serem dispensados tanto antes, quanto depois verbalmente e escrito, a ﬁm que a cicatrização seja a melhor possível, estando ciente de que cada pessoa possui um tempo especiﬁco e própria de reação. Veriﬁco que os materiais são devidamente esterilizados e lacrados como exige as posturas legais regulamentares, bem como veriﬁquei que os materiais são descartados após o atendimento.
        </p>
        <p>Assinatura do Responsável: _____________________________________ Data: ___ / ___ / _____ </p>
        <p>( ) Autorizo a perfuração do menor, estou ciente das instruções e obrigo-me a fazer segui-las.</p>
    </div>
    
</div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

//Close and output PDF document
$pdf->Output('Ficha_Anamnese.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>


