<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       trudax.tech
 * @since      1.0.0
 *
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Anamneseafdo
 * @subpackage Anamneseafdo/public
 * @author     Gustavo Texeira Rudiger <gustavo@trudax.tech>
 */
class Anamneseafdo_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Anamneseafdo_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Anamneseafdo_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/anamneseafdo-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Anamneseafdo_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Anamneseafdo_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/anamneseafdo-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Process the requests for full page display 
	 *
	 */
	public function process_requests(){

		if( $_GET["p"] == $this->plugin_name && $_GET['a'] == "anamnese"){
			$this->display_anamnese();
			exit;
		}
		
	}

	/**
	 * Display anamnese page
	 */
	public function display_anamnese(){
		ob_start();
		include plugin_dir_path( __FILE__ ) . '/partials/anamneseafdo-public-display.php';
		$html = ob_get_clean();
		
		echo $html;
	}
}
